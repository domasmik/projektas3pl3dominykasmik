﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class BaseService : DbConnection
    {
        public BaseService()
        {
        }

        protected void OpenConnecion()
        {
            if (libraryConnection.State == System.Data.ConnectionState.Closed)
            {
                libraryConnection.Open();
            }
        }

        protected void CloseConnection()
        {
            if (libraryConnection.State == System.Data.ConnectionState.Open)
            {
                libraryConnection.Close();
            }
        }
    }
}
