﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.SqlClient;
using Library.Domain.ViewModels;

namespace Library.Services
{
    public class UserServices : BaseService
    {

        public UserViewModel LoginViewModel(UserViewModel viewModel)
        {


            var LoginSelectCmdText = "Select * from User where(UserId,Username,Password) VALUES(@UserId,@Username,@Password)";

            var LoginSelectCmd = new MySqlCommand(LoginSelectCmdText, libraryConnection);
            LoginSelectCmd.Parameters.AddWithValue("@UserId", viewModel.UserId);
            LoginSelectCmd.Parameters.AddWithValue("@Username", viewModel.Username);
            LoginSelectCmd.Parameters.AddWithValue("@Password", viewModel.Password);


            OpenConnecion();
            // tempRoomUpdateCmd.ExecuteNonQuery();
            CloseConnection();

            return viewModel;
        }  
}
