﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class DbConnection
    {
        protected MySqlConnection libraryConnection { get; private set; }

        public DbConnection()
        {
            libraryConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["libraryConnection"].ConnectionString);
        }
    }
}
