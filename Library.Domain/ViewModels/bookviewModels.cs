﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Domain.ViewModels
{
    public class bookviewModels
    {

        public int Bid { get; set; }

        public String BName { get; set; }

        public string BAuthorName { get; set; }

        public int BPublished { get; set; }

        public String BCategories { get; set; }

        public String BTakeDate { get; set; }

        public String BBackDate { get; set; }

    }
}
