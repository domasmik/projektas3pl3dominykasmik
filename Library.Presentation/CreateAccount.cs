﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Library.Presentation
{
    public partial class CreateAccount : Form
    {
        string connectionString = @"Data Source = (Local\sqle2012; initial Catalog = UserRegistration; integrated Security = true;)";


        SqlConnection con = new SqlConnection(@"Data Source = (Local\sqle2012; initial Catalog = UserRegistration; integrated Security = true;");
        public CreateAccount()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
                MessageBox.Show("Please fill empty fields");
            else if (textBox2.Text != textBox3.Text)
                MessageBox.Show("Password do not match");
            else
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                 
 
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("Library", sqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@UserName", textBox1.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@PassWord", textBox1.Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    MessageBox.Show("Registration is successful");
                    Clear();


                }
                
        }
        void Clear()
        { textBox1.Text = textBox2.Text = textBox3.Text = "";
        }
    }
}
